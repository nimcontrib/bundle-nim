# Bundle Nim compiler and runtime with application

This package contains a standalone Nim distribution that can be bundled
with software dependent on Nim. In order to build Nim-related software,
use the `with-local-nim` script, e.g.:

        ./with-local-nim nim cc example.nim

The `with-local-nim` script will unpack and build the Nim compiler on
demand, then run the command it was given as an argument.

This tool uses zstd compression and contains a stripped down version
of the zstd command line program.

The current supported version of Nim is 1.0.0, but any other version
can be dropped in instead as long as it is in zstd format, and has a
filename of the form `nim-*.tar.zst`.

