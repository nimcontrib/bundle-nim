# Standalone zstd decompression tool

This is a stripped down version of the `zstd` compression/decompression
tool that only supports decompression. You can find the full version at
<https://github.com/facebook/zstd>. The code is based off of version
1.4.0 of the original code.

As with the full version, this version is provided as an open-sourced
dual [BSD](LICENSE) and [GPLv2](COPYING) licensed command line utility.

# Modifications

Aside from only including the necessary code for decompression, this
distribution includes the utility scripts `programs/build.sh` and
`unzstd.h`.

# Usage

In order to build `unzstd`, run `programs/build.sh`. You can also use
the `unzstd.sh` script as a drop-in replacement for `unzstd`, which will
build `unzstd` on demand and then execute it with the same arguments.
