#!/bin/sh
set -e
cd "$(dirname "$0")"

for CC in gcc clang cc; do
  command -v $CC >/dev/null 2>&1 || continue
  $CC -O -I../lib -I../lib/common -DXXH_NAMESPACE=ZSTD_ -I../lib/legacy -DZSTD_NOBENCH -DZSTD_NODICT -DZSTD_NOCOMPRESS ../lib/common/debug.c ../lib/common/entropy_common.c ../lib/common/error_private.c ../lib/common/fse_decompress.c ../lib/common/pool.c ../lib/common/threading.c ../lib/common/xxhash.c ../lib/common/zstd_common.c ../lib/decompress/huf_decompress.c ../lib/decompress/zstd_ddict.c ../lib/decompress/zstd_decompress.c ../lib/decompress/zstd_decompress_block.c zstdcli.c timefn.c util.c fileio.c -o ../unzstd && break
done
