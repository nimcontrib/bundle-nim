#!/bin/sh
set -e
dir="$(dirname "$0")"
test -x "$dir/unzstd" || sh "$dir"/programs/build.sh
exec "$dir"/unzstd "$@"

